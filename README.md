ncat
====================
n times output used to read from standard input

Usage
====================
	Usage: ncat [option] COUNT [FILE]

	Options:
		--help     show this help message and exit
		--version  print the version and exit

Example
====================
	$ seq 1 2 | ncat 2
	1
	2
	1
	2

	$ echo Hello | ncat 3
	Hello
	Hello
	Hello

License
====================
MIT License

Author
====================
Kusabashira <kusabashira227@gmail.com>
