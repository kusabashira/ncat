package main

import (
	"errors"
	"flag"
	"io"
	"io/ioutil"
	"os"
	"strconv"
)

func usage() {
	os.Stderr.WriteString(`
Usage: ncat [OPTION] COUNT [FILE]...
N times output used to read from standard input.

Options:
	--help       show this help message and exit
	--version    print the version and exit

Report bugs to <kusabashira227@gmail.com>
`[1:])
}

func version() {
	os.Stdout.WriteString(`ncat: v0.1.0
`)
}

func _main() error {
	var (
		isHelp    = flag.Bool("help", false, "show this help message")
		isVersion = flag.Bool("version", false, "print the version")
	)
	flag.Usage = usage
	flag.Parse()

	if *isHelp {
		usage()
		return nil
	}

	if *isVersion {
		version()
		return nil
	}

	var times int
	if flag.NArg() < 1 {
		return errors.New("no specify COUNT")
	}
	times, err := strconv.Atoi(flag.Arg(0))
	if err != nil {
		return err
	}

	var input io.Reader
	if flag.NArg() < 2 {
		input = os.Stdin
	} else {
		var inputs []io.Reader
		for _, fname := range flag.Args()[1:] {
			in, err := os.Open(fname)
			if err != nil {
				return err
			}
			defer in.Close()
			inputs = append(inputs, in)
		}
		input = io.MultiReader(inputs...)
	}

	in, err := ioutil.ReadAll(input)
	if err != nil {
		return err
	}

	for i := 0; i < times; i++ {
		os.Stdout.Write(in)
	}

	return nil
}

func main() {
	err := _main()
	if err != nil {
		os.Stderr.WriteString("ncat: ")
		os.Stderr.WriteString(err.Error())
		os.Stderr.WriteString("\n")
		os.Exit(1)
	}
	os.Exit(0)
}
